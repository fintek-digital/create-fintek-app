const questions = [
  {
    type: 'list',
    name: 'place',
    message: 'Choose client or backend',
    choices: [
      'client',
      'backend',
    ],
    default: 'backend',
  },
  
  {
      type: 'input',
      name: 'app',
      message: 'What is the application name',
      validate(value) {
          if (value.match(/^([a-z][a-z0-9]*)(-[a-z0-9]+)*\w+$/)) {
              return true;
          }
          
          return 'You must enter valid application (folder) name';
      },
  },

  {
    type: 'input',
    name: 'version',
    message: 'What is the application version',
    default: '0.0.0',
  },

  {
    type: 'input',
    name: 'description',
    message: 'description',
  },
];

module.exports = questions;
