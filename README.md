# Create Fintek App

Create fintek app is your first util to init your project.  

## Installation

Install globally with

```bash
npm install -g git@bitbucket.org:fintek-digital/create-fintek-app.git
```

Now you need to run the command anytime you want to start a new project

```bash
create-fintek-app
```

Choose a location (backend or frontend) and it will install the neede template

## Usage

after installation you can cd into project folder and run the commands

```bash
npm run dev
start development mode

npm start
start production mode

npm run docs
build docs and view them
```