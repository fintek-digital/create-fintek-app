#!/usr/bin/env node

const shell = require('shelljs');
const inquirer = require('inquirer');
const a = require('../app');
const questions = require('../config/questions');

const start = async () => {
  await a.checkVersions();
  a.startInfo();
  a.checkGit();

  inquirer.prompt(questions)
    .then((answers) => {
      const {
        place,
        app,
        version,
        description,
      } = answers;

      a.cloneTemplate(app, place);
      a.updateInfo(app, version, description);
      a.installPackages(app);
      a.endInfo(app);
    })
    .catch((error) => {
      if (error.isTtyError) {
        shell.echo('Prompt could not be rendered in the current environment'.red);
      } else {
        shell.echo('Something else went wrong'.red);
        shell.echo(`${error}`.red);
      }
    })
}

// start the app builder
start();