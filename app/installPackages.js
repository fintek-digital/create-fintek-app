const shell = require('shelljs');

/**
 * Install project packages
 * @param {string} app 
 */
 const installPackages = (app) => {
  shell.echo('\nInstall Packages (can take a few minutes...)');
  shell.exec(`cd ${app} && npm install`, { silent: true });
}

module.exports = installPackages;
