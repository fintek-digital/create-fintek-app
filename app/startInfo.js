const shell = require('shelljs');
const colors = require('colors');
const package = require('../package.json');

const startInfo = () => {
  shell.echo(`
  _____       __       __  
  / __(_)___  / /____  / /__
 / /_/ / __ \/ __/ _ \/ //_/
/ __/ / / / / /_/  __/ ,<   
/_/ /_/_/ /_/\__/\___/_/|_|  

create-fintek-app version ${package.version}`.green);
}

module.exports = startInfo;
