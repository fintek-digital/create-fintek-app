const shell = require('shelljs');
const colors = require('colors');

/**
 * Check for git on the machine
 * if not exists than terminate script
 */
 const checkGit = () => {
  if (!shell.which('git')) {
    shell.echo('Create-fintek-app requires git install on the machine'.red);
    shell.echo('For more deatils on installing git go to: https://git-scm.com/book/en/v2/Getting-Started-Installing-Git'.red);
    shell.exit(1);
  } else {
    const ver = shell.exec('git --version', { silent: true }).stdout;
    shell.echo(ver.green);
  }
}

module.exports = checkGit;