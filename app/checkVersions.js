const shell = require('shelljs');
const colors = require('colors');
const axios = require('axios').default;

const REMOTE_URL = 'https://bitbucket.org/fintek-digital/create-fintek-app/raw/main/package.json';

const convertVersionToNumber = function(version) {
  // Split a given version string into three parts.
  let parts = version.split('.');
  // Check if we got exactly three parts, otherwise throw an error.
  if (parts.length !== 3) {
    throw new Error('Received invalid version string');
  }
  // Make sure that no part is larger than 1023 or else it
  // won't fit into a 32-bit integer.
  parts.forEach((part) => {
    if (part >= 1024) {
      throw new Error(`Version string invalid, ${part} is too large`);
    }
  });
  // Let's create a new number which we will return later on
  let numericVersion = 0;
  // Shift all parts either 0, 10 or 20 bits to the left.
  for (let i = 0; i < 3; i++) {
    numericVersion |= parts[i] << i * 10;
  }
  return numericVersion;
};

const checkVersions = async () => {
  try {
    const json = shell.exec('npm outdated --global --json create-fintek-app', { silent: true }).stdout;
    const jsonParsed = JSON.parse(json);
    const current = jsonParsed['create-fintek-app']?.current || '0.0.0';

    // couldnt get latest from outdated output so taking it from repo
    const result = await axios.get(REMOTE_URL,{
      timeout: 3000,
    });
    const latest = result.data?.version || '0.0.0';

    const l = colors.green('|');

    if (convertVersionToNumber(current) < convertVersionToNumber(latest)) {
      shell.echo('\n');
      shell.echo(colors.green('========================================================================================='));
      shell.echo(`${l}                                                                                       ${l}`);
      shell.echo(`${l}                     Package create-fintek-app update available                        ${l}`);
      shell.echo(`${l}                     Update from version ${colors.blue(current)} to ${colors.blue(latest)}                                ${l}`);
      shell.echo(`${l}  Run ${colors.blue('npm install -g git@bitbucket.org:fintek-digital/create-fintek-app.git')} to update  ${l}`);
      shell.echo(`${l}                                                                                       ${l}`);
      shell.echo(colors.green('========================================================================================='));
      shell.exit(3);
    }
  } catch (error) {
    shell.echo(error);
    shell.exit(2);
  }
};

module.exports = checkVersions;
