const shell = require('shelljs');
const fs = require('fs');

const DEFAULT = {
  name: 'fintek app',
  version: '1.0.0',
  description: 'fintek app description',
}

/**
 * Update package json data
 * @param {string} app 
 * @param {string} version 
 * @param {string} description 
 */
 const updateInfo = (app, version, description) => {
  const filename = `./${app}/package.json`;
  const fileContent = fs.readFileSync(filename, 'utf-8');

  const jsonContent = updateJSONData(fileContent, {
    name: app || DEFAULT.name,
    version: version || DEFAULT.version,
    description: description || DEFAULT.description,
  });

  fs.writeFileSync(filename, jsonContent);
}

/**
 * update json with object data
 * @param {string} jsonContent 
 * @param {object} data 
 * @returns json data
 */
 const updateJSONData = (jsonContent, data) => {
  try {
    const json = JSON.parse(jsonContent);

    Object.keys(data).forEach((key) => {
      json[key] = data[key];
    })

    return JSON.stringify(json, null, 2);

  } catch (error) {
    shell.echo(error);
    shell.exit();
  } 
}

module.exports = updateInfo;