const shell = require('shelljs');
const colors = require('colors');

/**
 * Write end information
 */
 const endInfo = (app) => {
  shell.echo(`\nSuccess creating ${app} application`.green);
  shell.echo('inside the new directory you can:');
  shell.echo(`\n  npm run dev`.blue);
  shell.echo(`    start developemtn mode`);
  shell.echo(`\n  npm run start`.blue);
  shell.echo(`    start production mode`);
  shell.echo(`\n  npm run docs`.blue);
  shell.echo(`    create and view api docs`);
  shell.echo(`\n`);
  shell.echo(`To start working:`);
  shell.echo(`cd ${app}`.blue);
}

module.exports = endInfo;
