const startInfo = require('./startInfo');
const endInfo = require('./endInfo');
const checkGit = require('./checkGit');
const cloneTemplate = require('./cloneTemplate');
const installPackages = require('./installPackages');
const updateInfo = require('./updateInfo');
const checkVersions = require('./checkVersions');

module.exports = {
  startInfo,
  endInfo,
  checkGit,
  cloneTemplate,
  installPackages,
  updateInfo,
  checkVersions,
}