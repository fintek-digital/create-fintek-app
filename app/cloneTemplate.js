const shell = require('shelljs');

/**
 * Clone the app template into folder and prepare project
 * @param {string} app 
 * @param {string} place frontend or backend 
 */
 const cloneTemplate = (app, place) => {
  shell.echo(`\nClone ${place} project into ${app} folder`);
  const gitClone = shell.exec(`git clone https://bitbucket.org/fintek-digital/fintek-starter-${place}.git ${app}`, {silent: true});
  
  if (gitClone.code !== 0) {
    shell.echo(`Error: Git clone failed ${gitClone.code}`.red);
    shell.exit(1);
  }

  // remove git
  shell.rm('-rf', `${app}/.git`);

  if (place === 'backend') {
    // create .env from example
    shell.cp(`${app}/.env.example`, `${app}/.env`);
  }
}

module.exports = cloneTemplate;
